# RothC.R -- R implementation of the RothC model ( 
# http://www.rothamsted.ac.uk/sustainable-soils-and-grassland-systems/rothamsted-carbon-model-rothc ) 
# 
# Copyright (C) 2016 INRA - Manuel Martin 
# All rights reserved. 
# This file is part of the RothC R package. 
# 
# This RothC R package is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
# 
# RothC is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU General Public License for more details. 
# 
# You should have received a copy of the GNU General Public License 
# along with RothC R package.  If not, see <http://www.gnu.org/licenses/>. 
# Should you use this package, and please quote the publication
# Martin, M.P., Cordier, S., Balesdent, J., Arrouays, D.: Periodic solutions
# for soil carbon dynamics equilibriums with time-varying forcing variables.
# Ecol. Model. 204, 532-530 (2007), based on which the package was writen.



#

# clay <- 12
# b0 <- 1
# r0 <- 1
# g <- 1 - 1 / (1 + 1.44)  # crops
# f <- 12
# kd <- vs[1]/f
# kr <- vs[2]/f
# kb <- vs[3]/f
# kh <- vs[4]/f
# # lambda
# l <- 0
# 
# a <- 0.46 / (1 + xsplit(clay))
# b <- 0.54 / (1 + xsplit(clay))

#' @title propsCin
#'
#' @description Small function used to give the proportions of the carbon input
#' vector depending on the dpm:rpm ratio, and the proportion of farmyard
#' manure. Uses the default RothCs FYM composition. May be replaced by a custom
#' one using the locFYMSplit parameter.  
#' @param dpmToRpmValue : value of the dpm:rpm ratio
#' @param FYMProp : proportion of carbon coming from organic amendments
#' @param locFYMSplit : specific proportion of carbon associated to organic amendments.
#' Default is c(0.49, 0.49, 0, 0.02)
#' @return a dimension 4 vector
#' @export
#' @examples
#' propsCin(1.44, 0.05)
propsCin <- function(dpmToRpmValue, FYMProp, locFYMSplit = NULL){
	if (is.null(locFYMSplit)){
		locFYMSplit <- FYMSplit
	}

	a <- dpmToRpmValue/(1 + dpmToRpmValue)
	b <- 1 - a
	if (abs((a + b) - 1) > 1e-3){
		log_error("a+b should equal 1")
	}

	props <- (FYMProp * FYMSplit + (1 - FYMProp) * c(a, b, 0, 0))
	if (abs(sum(props) - 1) > 1e-3){


		log_error("props should equal 1")
	}
	return(props)
}
	
#' @title Solution for the steady state using the continous RothC formulation
#'
#' @description Solution for the steady state using the continous RothC formulation
#' @param b0 : carbon input (t/ha/month)
#' @param r0 : mean mineralization modifier
#' @param g : gamma, proportion of cin flowing to the DPM pool
#' @param e : epsilon, proportion of cin flowing to the HUM pool (default to 0)
#' @param clay (\%)
#' @param ts : stime step (currently only "month"), for backward compatibility
#' @return : a dimension 4 vector 
#' @export
#' @importFrom logger log_debug
#' @import logger
#' @examples
#' CeqContinuous(b0 = 1, r0 = 1, g = 0.33, clay = 12)
CeqContinuous <- function(b0, r0, g, e = 0, clay, ts = "month"){

	if (ts != "month") logger::log_error('time step other than month is
				    not currently supported')

 	log_level(DEBUG, paste0("b0 =", b0))
 	logger::log_debug(paste0("r0 =", r0))
 	logger::log_debug(paste0("g =", g))
 	logger::log_debug(paste0("e =", e))
	logger::log_debug(paste0("clay =", clay))
	
	# yearly decay				    
	f <- 12
	kd <- vs[1]/f
	kr <- vs[2]/f
	kb <- vs[3]/f
	kh <- vs[4]/f

	a <- 0.46 / (1 + xsplit(clay))
	b <- 0.54 / (1 + xsplit(clay))
	
	CU <- c(g / kd, 
		(1 - g - e) / kr,
		a / ((1 - a - b) * kb),
		(b - b * e + (1 - a) * e) / ((1 - a - b) * kh)
		)

	C0 <- (b0 / r0) * CU
	logger::log_debug(paste0("sum CU is :", sum(CU)))
	       
	C0
}	

#' @title Carbon input matching the steady state total SOC stock and average mineralization modifier (continuous form)
#'
#' @description Returns the carbon input given the steady state total SOC stock and average mineralization modifier
#' @param C0 : total SOC stock (t/ha)
#' @param r0 : mean mineralization modifier
#' @param g : gamma, proportion of cin flowing to the DPM pool
#' @param e : epsilon, proportion of cin flowing to the HUM pool (default to 0)
#' @param clay (\%)
#' @return : a scalar
#' @export
#' @examples
#' cinContinuous(50, r0 = 1, g = 0.58516, e = 0.001, clay = 20)
cinContinuous <- function(C0, r0, g, e = 0, clay){
	
	# yearly decay				    
	f <- 12
	kd <- vs[1]/f
	kr <- vs[2]/f
	kb <- vs[3]/f
	kh <- vs[4]/f

	a <- 0.46 / (1 + xsplit(clay))
	b <- 0.54 / (1 + xsplit(clay))

	b0 <- (C0 * r0) / 
		(g / kd + (1 - g) / kr + a / ((1 - a - b) * kb) + 
		(b - b * e + (1 - a) * e) / ((1 - a - b) * kh)
		)
	b0
}	


#' @title runExplicitRothC
#'
#' @description Uses the explicit solution of RothC to run in forward mode (mode =
#' "forward") or reverse mode (mode = "reverse"). If in forward mode, the size
#' of the output depends on the date sequence given in the nt argument. If in
#' reverse mode, the function estimates the total carbon input needed to reach
#' a given total stock (given the quality of the incoming organic matter, as
#' given by the gamma and epsilon parameters) after t monhs.  
#' @param C0tot total carbon at t=0 (excluding IOM), inverse mode only
#' @param Ctarget total carbon targetted after t months (inverse mode only)
#' @param clay clay content (\%)
#' @param g gamma, proportion of cin flowing to the DPM pool
#' @param e epsilon, proportion of carbon input flowing to the HUM pool (default to 0)
#' @param b0 monthly carbon input (tC/ha/month)
#' @param r0 average mineralization modifier
#' @param nt : vector of dates (in months) at which predictions will be made (for the forward mode)
#' @param t date to which the target stock refers (for the reverse mode)
#' @param mode "forward" or "inverse"
#' @return matrix(4xlength of nt)
#' @export
#' @examples
#' b0 <- runExplicitSol(Ctarget = 40, t = 30, clay = 12, g = 0.2, e = 0.1, r0 = 1, mode = "reverse")
#' CTar <- runExplicitSol(C0tot = 0, b0 = b0, nt = 30, clay = 12, g = 0.2, e = 0.1, r0 = 1, mode = "forward")
#' # Should yield 40 (initial CTarget)
#' sum(CTar)
#' @references Martin, M.P., Dimassi, B., Dobarco, M.R., Guenet, B., Arrouays, D., Angers, D.A., Blache, F., Huard, F., Soussana, J.-F., Pellerin, S., 2021. Feasibility of the 4 per 1000 aspirational target for soil carbon: A case study for France. Global Change Biology n/a. https://doi.org/10.1111/gcb.15547
runExplicitSol <- function(C0tot,
			   Ctarget,
			   clay,
			   g,
			   e = 0,
			   b0,
			   r0,
			   nt,
			   t, 
			   mode = "forward"){



	if (mode == "reverse"){
		b0 <- 1
	}

	f <- 12 # monthly time step
	kd <- vs[1]/f
	kr <- vs[2]/f
	kb <- vs[3]/f
	kh <- vs[4]/f

	a <- 0.46 / (1 + xsplit(clay))
	b <- 0.54 / (1 + xsplit(clay))

	# XA discriminant
	Delta <- (a - 1)^2 * kb^2 + (b - 1)^2 * kh^2 + 2 * (a + b + a * b - 1) * kb * kh
	ev <- c(-kd,
		-kr,
		((a - 1) * kb + (b - 1) * kh + Delta^0.5) / 2,
		((a - 1) * kb + (b - 1) * kh - Delta^0.5) / 2
		)
	names(ev) <- c("-kd", "-kr", "r+", "r-")
	rp <- as.numeric(ev["r+"])
	rms <- as.numeric(ev["r-"])
	
	#Eigen vectors
	Vd <- c(
		(kd + rp) * (kd + rms),
		0,
		a * kd * (kh - kd),
		b * kd * (kb - kd)
		)
	Vr <- c(
		0, 
		(kr + rp) * (kr + rms),
		a * kr * (kh - kr),
		b * kr * (kb - kr)
		)
	Vp <- c(
		0,
		0,
		a * kh,
		rp - (a - 1) * kb
		)
	Vm <- c(
		0,
		0,
		a * kh,
		rms - (a - 1) * kb
		)
	
	fd <- (kd + rp) * (kd + rms)
	fr <- (kr + rp) * (kr + rms)
	
	Td <- g / (kd * fd)
	Tr <- (1 - g - e) / (kr * fr)
	S1 <- 1 / ((1 - a - b) * kb) - g * (kh - kd) / fd - (1 - g - e) * (kh - kr) / fr
	S2 <- (b - b * e + (1 - a) * e) / ((1 - a - b) * kh) - g * b * (kb - kd) / fd - 
		b * (1 - g - e) * (kb - kr) / fr
	Tp <- (kh * S2 - (rms - (a - 1) * kb) * S1) / (kh * Delta^0.5)
	Tm <- ((rp - (a - 1) * kb) * S1 - kh * S2) / (kh * Delta^0.5)
	
	C0ss <- CeqContinuous(b0 = b0, r0 = r0, g = g, e = e, clay = clay)

	if (mode == "forward"){
		l <- C0tot / sum(C0ss)
		
		N <- length(nt)
		Ct <- matrix(NA, nrow = 4, ncol = N)
		for (i in 1:N){
			t <- nt[i]
			GU <- (
					Td * exp(- r0 * t * kd) * Vd +
					Tr * exp(- r0 * t * kr) * Vr +
					Tp * exp(r0 * t * rp) * Vp +
					Tm * exp(r0 * t * rms) * Vm
			     )

			Ct[, i] <-  C0ss + (b0 * (l - 1) / r0) * GU
		}
		log_debug(paste0("sumOfGU :", sum(GU)))
		log_debug(paste0("lambda :", l))
		log_debug(paste0("sumOfC0ss :", sum(C0ss)))
		return(Ct)
	}


	if (mode == "reverse"){
		GU <- Td * exp(- r0 * t * kd) * Vd +
			Tr * exp(- r0 * t * kr) * Vr +
			Tp * exp(r0 * t * rp) * Vp +
			Tm * exp(r0 * t * rms) * Vm
		C0StarU <- r0 * C0ss / b0
	
		b0f <- r0 * Ctarget / 
			( C0StarU[4] + C0StarU[1] + C0StarU[2] + C0StarU[3] -
			  (GU[1] + GU[2] +GU[3] + GU[4])
			)
		return(b0f)
	}
}


#
# clay <- 12
# b0 <- 1
# r0 <- 1
# g <- 1 - 1 / (1 + 1.44)  # crops
# C0Et <- CeqContinuous(b0 = b0, r0 = r0, g = g, clay = clay)
# 
# # Ad * Vd - (l - 1) * C0Et
# 
# 
# # (Td *  Vd + Tr * Vr + Tp * Vp + Tm * Vm) / C0Et
# # 
# N <- 1e6
# l <- 0
# CsOld <- CDynDiscrete(C0Et * l, dpmRpmRat = 1.44, rho = 1, clay = clay, cin = 1, N)
# 
# # CeqDiscrete(argi = 12, rho = 1, dpmRpmRat = 0.4 / 0.6, cin = 1)
# # continuousEquilibrium(arg = 12, singleRhos = rep(1, 12), cin = rep(1,12), coverType = "crops")
# # C0Et 
# 
# # Cs <- CsOld
# # for (i in 1:N){
# # 	t <- i - 1
# # 	Ct <-  C0Et + 
# # 		((l - 1) / r0) * 
# # 		(
# # 		Td * exp(- r0 * t * kd) * Vd +
# # 		Tr * exp(- r0 * t * kr) * Vr +
# # 		Tp * exp(r0 * t * rp) * Vp +
# # 		Tm * exp(r0 * t * rms) * Vm
# # 		)
# # 	Cs[, i] <- Ct
# # }
# Cs <- runExplicitSol(C0tot = l * sum(C0Et), b0 = b0,
# 	       nt = 1:N, clay = clay, 
# 	       g = g, e = 0,
# 	       r0 = r0, mode = "forward")
# 


# 
# sum(Cs[, 31])
# Cs# <- Ct
# 
# sum(Cs[, N] - CsOld[, N])
# plot(colSums(Cs)[1:N])
# lines(colSums(CsOld)[1:N])
# i <- 4
# plot(Cs[4, 1:N], ylim = range(c(Cs[4, 1:N], CsOld[4, 1:N])))
# lines(CsOld[4, 1:N])
# # constants
# Ad <- (l - 1)
# 
# Cs - Csold
# 
# # Crpm1
# dpmt <- function(x){exp(-kr) * x + g}
# dpmt(dpmt(dpmt(96)))
# CsOld[, 1:4]
# Cs[, 1:3]
# Frothc(rho = 1, argi = clay)

# from the analytical solution
# par(mfrow = c(1,2))
# rpmt <- function(x){(1 - g) * (1 + (l - 1) * exp(-x * kr)) / kr}
# curve((1 - g) * (1 + (l - 1) * exp(-x * kr)) / kr, 0, 1000)
# abline(h = 94.426)
# rpmt(0)
# points(CsOld[2, 2:N])
# 
# par(mfrow = c(2,2))
# for (i in 1:4){
# 	plot(CsOld[i, 2:1000], ylim = range(c(CsOld[i,], Cs[i, ])), main = i, col = "#34225640")
# 	lines(Cs[i, 1:1000])
# }
# rpmt(1000)
# 
# (1 - g) * (1 + (l - 1) * exp(-1000 * kr)) / kr
# 
# 
