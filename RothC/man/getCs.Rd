% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/RothC.R
\name{getCs}
\alias{getCs}
\title{Returns the *c* component of the mineralization modifier}
\usage{
getCs(landUse = NULL, nYears = 1, cover = NULL)
}
\arguments{
\item{landUse}{: in 1, 2, 3 or 7. If cover not provided, gives defalut values for
estimating default covers throughout the year}

\item{nYears}{: duration of the simulation (in year)}

\item{cover}{: vectors of 0 and/or 1 representing monthly soil cover}
}
\value{
a vector
}
\description{
Returns the *c* component of the mineralization modifier as defined 
in the RothC model documentation, as a function of a landuse and for a given
number of years.
}
\references{
Falloon, P., Smith, P., Coleman, K., Marshall, S., 1998. Estimating the size of the inert organic matter pool from total soil organic carbon content for use in the Rothamsted carbon model. SOIL BIOLOGY & BIOCHEMISTRY 30, 1207-1211. doi:10.1016/S0038-0717(97)00256-3
}
