#!/bin/bash - 
#===============================================================================
#
#          FILE: buiInfosolRPackage.sh
# 
#         USAGE: ./buiInfosolRPackage.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Manuel Martin, 
#  ORGANIZATION: 
#       CREATED: 06/05/2014 14:09
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

pckgeName=RothC

pckgeRoot=./


cd $pckgeRoot
echo '  '
echo '  '
echo '________________2     removing *.Rd files in '
echo $pckgeRoot/$pckgeName'/man'
rm $pckgeRoot/$pckgeName/man/*.Rd


## NB for testing : 
## devtools::test() should be run by hand first in order to make sure tests pass
## methods is required here in order to roxygenize to work properly from Rscript
echo '\n\n\n________________2bis     cleaning R code'

for file in $pckgeRoot/$pckgeName/R/*; do
	echo "removing require()s and library()s in "$file
	sed -i "s/require(.*)//" $file
	sed -i "s/library(.*)//" $file
done

# inline and Rcpp don't work like that so disable checks on it
sed -i "s/.*cpp = 1.*/#&/" $pckgeRoot/$pckgeName/tests/testthat/test-all.R

cd $pckgeRoot
echo '  '
echo '  '
echo '________________3     ROxygenase'
Rscript -e 'getwd()';
Rscript -e 'require(roxygen2); devtools::document("../../RothC")';

## checks and build the package
echo '  '
echo '  '
echo '________________4     Build'
R CMD build RothC

## checks and build the package
echo '  '
echo '  '
echo '________________4     Check'
R CMD check RothC_$1.tar.gz


