# RothC

Implements in R the Rothamsted RothC package. Adds useful functions.

## Installation

### Command line

1. Create a temporary folder

```
mkdir /tmp/rothc.git
cd /tmp/rothc.git
```

2. Clone the project and build the archive

```
git clone https://forgemia.inra.fr/manuel.martin/rothc.git
cd rothc
tar -cvf RothC.tar.gz RothC/
```

3. Install the package from the archive

```
R CMD INSTALL RothC.tar.gz
```

### Downloading the Archive 

Download the *.tar.gz file at [https://forgemia.inra.fr/manuel.martin/rothc/-/wikis/RothC](https://forgemia.inra.fr/manuel.martin/rothc/-/wikis/RothC) and install it using your prefered way.
